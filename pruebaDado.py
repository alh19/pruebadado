import random
num1 = random.randint(1,6)
num2 = random.randint(1,6)
num3 = random.randint(1,6)
print("El resultado del dado 1 es: ", num1)
print("El resultado del dado 2 es: ", num2)
print("El resultado del dado 3 es: ", num3)
if num1==6 and num2==6 and num3==6:
    print("ORO")
if num1==6 and num2==6 or num1==6 and num3==6 or num2==6 and num3==6:
    print("PLATA")
if num1==6 or num2==6 or num3==6:
    print("BRONCE")
else:
    print("SIN PREMIO")
print("La puntuacion total es", num1+num2+num3)