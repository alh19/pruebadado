package pruebadado;

import java.util.Scanner;
import java.util.Random;
/**
 *
 * @author alber
 */
public class PruebaDado {
    
    public static void main(String[] args) {
        
        Scanner entrada = new Scanner(System.in);
        Random r = new Random();
        int num1 = r.nextInt(6)+1;
        int num2 = r.nextInt(6)+1;
        int num3 = r.nextInt(6)+1;
        int total = num1+num2+num3;
        
        System.out.println("El primer dado es: " + num1);
        System.out.println("El segundo dado es: " + num2);
        System.out.println("El tercer dado es: " + num3);
        if(total==18){
            System.out.println("ORO");
        }
        if((num1==6 && num2==6) || (num1==6 && num3==6) || (num2==6 && num3==6)){
            System.out.println("PLATA");
        }
        if(num1==6 || num2==6 || num3==6){
            System.out.println("BRONCE");
        }
        System.out.println("SIN PREMIO");
        
        System.out.println("El total es: " + total);
        
    }
    
}
